<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Check user</title>
</head>
<body>
<header>
    <h1>Check user</h1>
</header>
<main>
    <article>
        <form method="post" action="script.php">
            <label>Username <input type="text" name="username"></label>
            <input type="submit" value="Check!">

            <?php
            if (isset($_SESSION["user_unknown"])) {
                //Ojej, użytkownik nie istnieje, daje znać :(
                echo '<p style="color:red;">User is not found!</p>';
                unset($_SESSION["user_unknown"]);
            }
            if (isset($_SESSION["user_known"])) {
                //Yay! Mam użytkownika, daje znać :D
                echo '<p style="color:green;">User has been found!</p>';
                echo '<p>Username: <b>'.$_SESSION["username"].'</b><br>';
                echo 'ID number: <b>'.$_SESSION["idnum"].'</b><br>';
                echo 'Adress e-mail: <b>'.$_SESSION["email"].'</b><br>';
                echo 'Date of register: <b>'.$_SESSION["regdate"].'</b></p>';
                unset($_SESSION["user_known"]);
                unset($_SESSION["username"]);
                unset($_SESSION["idnum"]);
                unset($_SESSION["email"]);
                unset($_SESSION["regdate"]);
            }
            ?>

        </form>
    </article>
</main>
</body>
</html>
